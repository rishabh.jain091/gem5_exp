#!/usr/bin/env python
fname = input("> Which file? ")
inj_rate = input("> Corresponding injection rate? ")
with open(fname) as f:
  content = f.readlines()

content = [x.strip() for x in content]
lis = []
for x in content:
  lis.append(int(x))

print(lis)

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


minimum = min(lis)
maximum = max(lis)
print("Total packets: " + str(len(lis)))
print("The max latency is : " + str(maximum) + " Cycles")
print("The min latency is : " + str(minimum) + " Cycles")
legend = ['injection rate = ' + inj_rate]
bins = maximum - minimum
plt.hist(lis,bins)
plt.xlabel("Latency ")
plt.ylabel("Number of packets")
plt.legend(legend)
plt.show()
